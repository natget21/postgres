package main

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

var db *sql.DB //used to store connection object

func dbConn() (db *sql.DB) {
	db, err := sql.Open("postgres", "postgres://postgres:1361@localhost/restaurantdb?sslmode=disable")

	if err != nil {
		panic(err)
	}

	if err = db.Ping(); err != nil {
		panic(err)
	}

	//defer db.Close()
	fmt.Println("DB Connected sucessfully !")
	return db

}

func init() {
	db = dbConn()
}

//crud operations
func Insert(db *sql.DB) int {
	//insert
	sqlStatement := `INSERT INTO users (uuid,full_name, email, phone, password)VALUES ($1, $2, $3, $4, $5)RETURNING id`
	id := 0
	var err = db.QueryRow(sqlStatement, 1, "natnael getachew", "natget21@gmail.com", "+251921", "password").Scan(&id)
	if err != nil {
		panic(err)
	}
	return id
}

func Retrive(db *sql.DB) (int, string, string) {
	//retrive
	sqlStatement := `SELECT id, email, phone FROM users WHERE id=$1;`
	var email string
	var id int
	var phone string

	row := db.QueryRow(sqlStatement, 1)
	switch err := row.Scan(&id, &email, &phone); err {
	case sql.ErrNoRows:
		fmt.Println("No rows were returned!")
		return 0, "", ""
	case nil:
		return id, email, phone
	default:
		panic(err)
	}
}

func Update(db *sql.DB) bool {
	//update
	sqlStatement := `UPDATE users SET phone = $2 WHERE id = $1;`

	_, err := db.Exec(sqlStatement, 1, "+251911121314")
	if err != nil {
		panic(err)
	}

	return true
}

func Delete(db *sql.DB) bool {
	// //delete
	sqlStatement := `DELETE FROM users WHERE id = $1;`
	_, err := db.Exec(sqlStatement, 1)
	if err != nil {
		panic(err)
	}

	return true
}

func main() {

	//i:= Insert(db)
	//println("i = ",i)

	// i, e, p := Retrive(db)
	// println(i, e, p)

	// b := Update(db)
	// i, e, p := Retrive(db)
	// println(b, i, e, p)

	// d := Delete(db)
	// i, e, p := Retrive(db)
	// println(d, i, e, p)

	db.Close()

}
