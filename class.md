


download - from the postgress site 
    - https://www.postgresql.org/download/windows/
    - https://www.postgresql.org/download/macosx/
    - https://www.postgresql.org/download/linux/ubuntu/

    optionaly pgadmin - https://www.pgadmin.org/download/

download go connector
    - go get github.com/lib/pq


sudo su postgres
psql

CREATE DATABASE restaurantdb;  - > create 

\l - >list

psql -d restaurantdb -U postgres -W -> connect to db

\c template1 postgres - > switch to database

drop database restaurantdb;


// create connection in pgadmin


//--------usefull commands-------------

\?: List all the commands
\l: List databases
\conninfo: Display information about current connection
\c <Database Name>: Connect to a database
\d <Table Name>: Show table definition including triggers
\dt: List all tables
\d+ <Table Name>: More detailed table definition including description and physical disk size
\l: List databases
\x: Pretty-format query results instead of the not-so-useful ASCII tables
\du: List users
\du <username>: List a username if present
\dn: List available schema
\e: Edit command in your own editor
\q: Quit/Exit

//--------usefull commands-------------



//add database restaurant.sql
command -> psql –f restaurant.sql –d restaurantdb

or copy to pgadmin query


by default postgress 
    - creates a schema named public and 
    - grant role named pubic and there fore can create object in public schemas

This becomes a problem if you are trying to create a read-only user.

REVOKE CREATE ON SCHEMA public FROM PUBLIC; -> remove create permission on public schema

REVOKE ALL ON DATABASE restaurantdb FROM PUBLIC; -> remove public role ability to connect to the database


//readonly
CREATE ROLE readonly; -> create role named readonly
GRANT CONNECT ON DATABASE restaurantdb TO readonly; -> grand the role to connect to the database 
GRANT USAGE ON SCHEMA public TO readonly; -> grand to use the public schema
GRANT SELECT ON TABLE categories, menus TO readonly; -> grand select query only on the listed tables
    // GRANT SELECT ON ALL TABLES IN SCHEMA public TO readonly; -> for all tables 
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO readonly; -> alter future created tables to be accessable by readonly role


//read write
CREATE ROLE readwrite;
GRANT CONNECT ON DATABASE restaurantdb TO readwrite; -> grand the role to connect to the database 
GRANT USAGE, CREATE ON SCHEMA public TO readwrite; -> grand to use and create the public schema
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE comments, orders TO readwrite;
    // GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO readwrite;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO readwrite;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA public TO readwrite;

//create a user
CREATE USER app_user WITH PASSWORD 'P@$$w0RrdD';
GRANT readwrite TO app_user;